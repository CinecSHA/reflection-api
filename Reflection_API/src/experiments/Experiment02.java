package experiments;

public class Experiment02 {
	
	public float n = 10.0f;
	private float m = 20.0f;

	public Experiment02 () {
	}
		
		public Experiment02(float n, float m) {
			this.n = n;
			this.m = m;
		}
		
		public float getA() {
		    return n;
		}
		
		
		public float getB() {
		    return m;
		}
		
		public void setB(float m) {
			this.m = m;
		}
		
		public String toString() {
		return String.format("(a:%.1f, b:%.1f)", n, m);
	}
}
	
