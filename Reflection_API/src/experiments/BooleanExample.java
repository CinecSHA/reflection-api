package experiments;

public class BooleanExample {
	private boolean flag;

	public BooleanExample(boolean flag) {
		this.flag = flag;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
