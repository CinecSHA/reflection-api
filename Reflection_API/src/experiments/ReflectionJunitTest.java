package experiments;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReflectionJunitTest {

	@Test
	public void reflectionResult() {
		Experiment junit = new Experiment(10, 20);

		assertEquals(10, junit.getA());
		assertEquals(20, junit.getB());

	}

	public int squareA(int num) {
	    return num * num;
	
	}
		@Test
		public void testSquare() {
		    int result = squareA(10);
		    int expected = 100;
		    assertEquals(expected, result);
		}
}
	