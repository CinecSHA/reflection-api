package experiments;

import static org.junit.Assert.*;

import org.junit.Test;

public class BooleanExampleTest {

	@Test
	public void testBooleanExample() {

		BooleanExample example = new BooleanExample(true);
		assertTrue(example.isFlag());

		example.setFlag(true);
		assertFalse(example.isFlag());
	}
}
