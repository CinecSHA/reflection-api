package experiments;

public class Experiment {
	public int a = 10;
	private int b = 20;
	
	public double  x = 10.55;
	private double  y = 20.35;
	
	public float n = 10.0f;
	private float m = 20.0f;

	public Experiment() {
	}

	public Experiment(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public void squareA() {
		this.a *= this.a;
	}

	private void squareB() {
		this.b *= this.b;
	}

	public int getA() {
		return a;
	}

	private void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}
	
	public String toString() {
		return String.format("(a:%d, b:%d)", a, b);
	}
}


