package experiments;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReflectionJunitTest2 {

	@Test
	public void flotTypeTest() {
		Experiment02 junit = new Experiment02 (2.0f, 4.0f);
		float result = junit.getA();
		float expected = 2.0f;
	    assertEquals(expected, result, 0.0f);
		
	}

}
