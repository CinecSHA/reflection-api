package experiments;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReflectionJunitTest03 {

	@Test
	public void testSqureA() {
		Experiment03 junit = new Experiment03(10, 20);
        junit.squareA();
        assertEquals(100.0, junit.getA(), 0.0);
    }

    @Test
    public void testGetB() {
    	Experiment03 junit = new Experiment03 (10, 20);
        assertEquals(20.0, junit.getB(), 0.0);
    }
}
	


