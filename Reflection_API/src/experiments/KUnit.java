package experiments;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class KUnit {

	  private static List<String> checks;
	  private static int checksMade = 0;
	  private static int passedChecks = 0;
	  private static int failedChecks = 0;

/* */

private static void addToReport(String txt) {
    if (checks == null) {
      checks = new LinkedList<String>();
    }
    checks.add(String.format("%04d: %s", checksMade++, txt));
  }


public static void checkEquals(int value1, int value2) {
    if (value1 == value2) {
      addToReport(String.format("  %d == %d", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %d == %d", value1, value2));
      failedChecks++;
    }
}
public static void checkNotEquals(int value1, int value2) {
    if (value1 != value2) {
      addToReport(String.format("  %d != %d", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %d != %d", value1, value2));
      failedChecks++;
    }
  }
}